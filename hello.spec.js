
test("should pass", ()=>{
    expect(100).toBe(100)
})

test("should also pass", ()=>{
    expect(500).toBe(500)
})

test("should pass too", ()=>{
    expect(50).toBe(50)
})
